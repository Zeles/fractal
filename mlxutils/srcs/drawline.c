/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawline.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/21 20:45:15 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 11:48:23 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlxutils.h"

static void	drawlinelow(t_image *img, t_ivector2d start,
t_ivector2d end, t_rgb color)
{
	t_ivector	delta;
	int			error;
	t_ivector2d	cord;
	int			diry;

	delta = setivector(abs(start.x - end.x), abs(start.y - end.y),
	abs(start.y - end.y));
	error = 0;
	diry = end.y - start.y;
	diry > 0 ? diry = 1 : 0;
	diry < 0 ? diry = -1 : 0;
	cord = start;
	while (cord.x < end.x)
	{
		put_pixel_to_image(img, cord, color);
		error = error + delta.z;
		if (2 * error >= delta.x)
		{
			cord.y = cord.y + diry;
			error = error - delta.x;
		}
		cord.x++;
	}
}

static void	drawlinehight(t_image *img, t_ivector2d start,
t_ivector2d end, t_rgb color)
{
	t_ivector	delta;
	int			error;
	t_ivector2d	cord;
	int			dirx;

	delta = setivector(abs(start.x - end.x), abs(start.y - end.y),
	abs(start.x - end.x));
	error = 0;
	dirx = end.x - start.x;
	dirx > 0 ? dirx = 1 : 0;
	dirx < 0 ? dirx = -1 : 0;
	cord = start;
	while (cord.y < end.y)
	{
		put_pixel_to_image(img, cord, color);
		error = error + delta.z;
		if (2 * error >= delta.x)
		{
			cord.x = cord.x + dirx;
			error = error - delta.y;
		}
		cord.y++;
	}
}

void		drawline(t_image *img, t_ivector2d start,
t_ivector2d end, t_rgb color)
{
	if (abs(end.y - start.y) < abs(end.x - start.x))
	{
		if (start.x > end.x)
			drawlinelow(img, end, start, color);
		else
			drawlinelow(img, start, end, color);
	}
	else
	{
		if (start.y > end.y)
			drawlinehight(img, end, start, color);
		else
			drawlinehight(img, start, end, color);
	}
}
