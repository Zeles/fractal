/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_pixel_to_image.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 19:53:56 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 12:50:17 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlxutils.h"

void	put_pixel_to_image(t_image *img, t_ivector2d cord, t_rgb color)
{
	if (cord.x > 0 && cord.y > 0 &&
	cord.x < img->size.x && cord.y < img->size.y)
		img->line[cord.x + (cord.y * img->size.x)] = convertcolor(color);
}
