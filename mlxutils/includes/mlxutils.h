/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlxutils.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 11:48:42 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 15:45:43 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MLXUTILS_H
# define MLXUTILS_H
# include <mlx.h>
# include <math.h>
# include "../../ft_graphics/includes/vector.h"
# include "../../ft_graphics/includes/color.h"
# include "../../libft/libft.h"

typedef struct	s_window
{
	void		*mlx_ptr;
	void		*window;
	t_ivector2d	size;
	char		*title;
}				t_window;

t_window		*createwindow(void *mlx_ptr, t_ivector2d size, char *title);
void			editwindow(t_window **window, t_ivector2d size, char *title);
void			destroywindow(t_window **window);

typedef struct	s_image
{
	void		*img_ptr;
	t_ivector2d	size;
	int			*line;
	int			bpp;
	int			size_line;
	int			endian;
}				t_image;

t_image			*createimage(void *mlx, t_ivector2d size);
void			resizeimage(void *mlx, t_image **img, t_ivector2d newsize);
void			destroyimage(t_image **img);

void			clearimage(t_image *img);
void			put_pixel_to_image(t_image *img, t_ivector2d cord, t_rgb color);

int				convertcolorptr(t_rgb *color);
int				convertcolor(t_rgb color);

void			drawline(t_image *img, t_ivector2d start,
t_ivector2d end, t_rgb color);
#endif
