/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawui.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/13 20:26:34 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 14:59:00 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

void	drawui(t_fractal *fra)
{
	mlx_string_put(fra->win->mlx_ptr, fra->win->window, 10, 10, 0x00FFFFFF,
	"Max Iteration:");
	mlx_string_put(fra->win->mlx_ptr, fra->win->window, 150, 10, 0x00FFFFFF,
	ft_itoa(fra->maxiter));
	mlx_string_put(fra->win->mlx_ptr, fra->win->window, 10, 25, 0x00FFFFFF,
	"Zoom:");
	mlx_string_put(fra->win->mlx_ptr, fra->win->window, 75, 25, 0x00FFFFFF,
	ft_itoa(fra->zoomiter));
	mlx_string_put(fra->win->mlx_ptr, fra->win->window, 10, 40, 0x00FFFFFF,
	"Color: C");
	mlx_string_put(fra->win->mlx_ptr, fra->win->window, 10, 55, 0x00FFFFFF,
	"Resize window: R");
	mlx_string_put(fra->win->mlx_ptr, fra->win->window, 10, 70, 0x00FFFFFF,
	"Switch type: T");
}
