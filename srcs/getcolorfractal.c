/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getcolorfractal.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 11:46:20 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 11:46:27 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

t_setcolor	setcolor(t_rgb cmi, t_rgb start, t_rgb end)
{
	t_setcolor	result;

	result.colormaxiter = cmi;
	result.start = start;
	result.end = end;
	return (result);
}

t_rgb		getcolorfractal(t_fractal *fra, int iter)
{
	if (iter == fra->maxiter)
		return (fra->colors[fra->setcol].colormaxiter);
	else
	{
		if (iter <= fra->maxiter - 1 && iter >= fra->maxiter / 2)
			return (dlerpcolor(fra->colors[fra->setcol].start,
			fra->colors[fra->setcol].end, (double)(1.0f
			/ fra->maxiter) * iter));
		else
			return (dlerpcolor(fra->colors[fra->setcol].end,
			fra->colors[fra->setcol].start, (double)(1.0f
			/ fra->maxiter) * iter));
	}
}
