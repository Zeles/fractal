/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawmolden.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/11 13:29:27 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/14 19:24:04 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

static t_rgb	calciter(t_fractal *fra, t_dvector2d p)
{
	t_dvector2d z;
	t_ivector2d iter;

	z = p;
	iter.x = 0;
	iter.y = fra->maxiter;
	while (iter.x < iter.y)
	{
		z = setdvector2d(z.x * z.x - z.y * z.y + p.x,
		2 * z.x * z.y + p.y);
		if (z.x * z.x + z.y * z.y > 4)
			break ;
		iter.x++;
	}
	return (getcolorfractal(fra, iter.x));
}

static void		crutch(t_fractal *fra, int iter,
t_dvector2d *inc, t_dvector2d *new)
{
	new->y = fra->maxmin.min.y + ((fra->maxmin.max.y - fra->maxmin.min.y)
	/ THREAD) * iter;
	inc->x = (fra->maxmin.max.x - fra->maxmin.min.x) / fra->img->size.x;
	inc->y = (fra->maxmin.max.y - fra->maxmin.min.y) / fra->img->size.y;
}

void			*calcmolden(void *arg)
{
	t_dvector2d inc;
	t_ivector2d cord;
	t_dvector2d new;
	t_thread	*t;

	t = (t_thread*)arg;
	crutch(t->fra, t->iter, &inc, &new);
	cord.y = (t->fra->win->size.y / THREAD) * t->iter;
	while (cord.y < (t->fra->win->size.y / THREAD) * (t->iter + 1))
	{
		cord.x = 0;
		new.x = t->fra->maxmin.min.x;
		while (cord.x < t->fra->img->size.x)
		{
			put_pixel_to_image(t->fra->img, cord, calciter(t->fra, new));
			cord.x++;
			new.x += inc.x;
		}
		cord.y++;
		new.y += inc.y;
	}
	return (0);
}
