/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculatefractal.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/12 13:22:46 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 14:51:53 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

void	checkthread(t_fractal *fra, void *(cal)(void*))
{
	t_thread	thread[THREAD];
	int			t;

	t = 0;
	while (t < THREAD)
	{
		thread[t].iter = t;
		thread[t].fra = fra;
		pthread_create(&thread[t].thread, NULL, cal, &thread[t]);
		t++;
	}
	t = 0;
	while (t < THREAD)
	{
		pthread_join(thread[t].thread, NULL);
		t++;
	}
	draw(fra);
}

void	calculatefractal(t_fractal *fra)
{
	if (fra->type == MALDENBROT)
		checkthread(fra, calcmolden);
	else if (fra->type == JULIA)
		checkthread(fra, calcjulia);
	else if (fra->type == SHIPBURN)
		checkthread(fra, calcshipburn);
}
