/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 16:53:48 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 15:50:24 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

void	usage(char *argv1)
{
	ft_putstr("Usage: ");
	ft_putstr(argv1);
	ft_putendl(" maldenbrot or julia or shipburn");
	exit(EXIT_FAILURE);
}

void	error(char *str)
{
	ft_putendl(str);
	exit(EXIT_FAILURE);
}
