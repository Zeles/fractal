/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zoom.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 14:37:47 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 14:41:27 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

static t_dvector	zoompoint(t_fractal *fra)
{
	t_dvector result;

	result.x = fra->mouse.x / (fra->win->size.x / (fra->maxmin.max.x -
	fra->maxmin.min.x)) + fra->maxmin.min.x;
	result.y = fra->mouse.y / (fra->win->size.y / (fra->maxmin.max.y -
	fra->maxmin.min.y)) + fra->maxmin.min.y;
	result.z = fra->mouse.z;
	return (result);
}

static void			apzoom(t_fractal *fra, float zoom)
{
	t_dvector	result;
	float		inter;

	inter = 1.0f / zoom;
	fra->maxmin.min.x = flerp(fra->mouse.x, fra->maxmin.min.x, inter);
	fra->maxmin.min.y = flerp(fra->mouse.y, fra->maxmin.min.y, inter);
	fra->maxmin.max.x = flerp(fra->mouse.x, fra->maxmin.max.x, inter);
	fra->maxmin.max.y = flerp(fra->mouse.y, fra->maxmin.max.y, inter);
}

void				zoom(int key, int x, int y, t_fractal *fra)
{
	if (key == 5 || key == 4)
	{
		if (key == 5)
		{
			if (fra->zoomiter < 149)
			{
				fra->zoomiter++;
				fra->mouse = setdvector(x, y, 1.1f);
				fra->mouse = zoompoint(fra);
			}
		}
		else if (key == 4)
		{
			if (fra->zoomiter > -29)
			{
				fra->zoomiter--;
				fra->mouse = setdvector(x, y, 0.9f);
				fra->mouse = zoompoint(fra);
			}
		}
		apzoom(fra, fra->mouse.z);
		fra->mouse.z = 1;
	}
}
