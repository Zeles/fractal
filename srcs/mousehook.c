/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mousehook.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 17:03:19 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 15:44:46 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

int			closewindow(void *param)
{
	t_fractal *fra;

	fra = (t_fractal*)param;
	destroyimage(&fra->img);
	destroywindow(&fra->win);
	exit(EXIT_SUCCESS);
	return (0);
}

t_dvector2d	getaxismouse(t_dvector2d vec, t_dvector2d center, t_dvector2d div)
{
	vec.x = (vec.x - center.x) / div.x;
	vec.y = (vec.y - center.y) / div.y;
	return (vec);
}

int			mousehook(int key, int x, int y, void *param)
{
	t_fractal	*fra;

	fra = (t_fractal*)param;
	zoom(key, x, y, fra);
	if (fra->type == JULIA)
		if (key == 1)
			fra->juliastart = getaxismouse(setdvector2d(x, y),
			setdvector2d(fra->win->size.x / 2, fra->win->size.y / 2),
			setdvector2d(fra->win->size.x / 2, fra->win->size.y / 2));
	calculatefractal(fra);
	draw(fra);
	return (0);
}
