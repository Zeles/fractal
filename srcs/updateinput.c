/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   updateinput.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 17:01:53 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 15:43:41 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

void	updateinput(t_fractal *fra)
{
	mlx_mouse_hook(fra->win->window, mousehook, fra);
	mlx_hook(fra->win->window, 17, 1, closewindow, fra);
	mlx_hook(fra->win->window, 2, 3, keyhook, fra);
}
