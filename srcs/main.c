/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 16:49:22 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 16:04:42 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

enum e_fractaltype	validfractel(char *str)
{
	if (ft_strcmp(str, "maldenbrot") == 0)
		return (MALDENBROT);
	else if (ft_strcmp(str, "julia") == 0)
		return (JULIA);
	else if (ft_strcmp(str, "shipburn") == 0)
		return (SHIPBURN);
	error("Error");
	return (0);
}

void				createcolor(t_fractal *fra)
{
	fra->colors[0] = setcolor(setrgb(0, 0, 0), setrgb(255, 0, 0),
	setrgb(0, 0, 0));
	fra->colors[1] = setcolor(setrgb(224, 117, 11), setrgb(224, 117, 11),
	setrgb(0, 0, 0));
	fra->colors[COLORS - 1] = setcolor(setrgb(0, 0, 0), setrgb(0, 0, 255),
	setrgb(0, 0, 0));
}

void				createfractaldata(t_fractal *fra)
{
	if (fra->type == MALDENBROT || fra->type == SHIPBURN)
	{
		fra->maxmin.max = setdvector2d(1, 1);
		fra->maxmin.min = setdvector2d(-2, -1);
	}
	if (fra->type == JULIA)
	{
		fra->maxmin.max = setdvector2d(1.5f, 1);
		fra->maxmin.min = setdvector2d(-1.5f, -1);
	}
	fra->juliastart = setdvector2d(0.5f, 0.7f);
}

t_fractal			*init(char *argv1)
{
	void		*mlx_ptr;
	t_fractal	*result;

	if (!(result = (t_fractal*)malloc(sizeof(t_fractal))))
		error("Memory is not allocated");
	mlx_ptr = mlx_init();
	result->type = validfractel(argv1);
	result->win = createwindow(mlx_ptr, setivector2d(800, 600), "Fractal");
	result->img = createimage(mlx_ptr, setivector2d(800, 600));
	updateinput(result);
	result->mouse = setdvector(0, 0, 1);
	result->maxiter = 30;
	result->zoomiter = 0;
	result->setcol = 0;
	createcolor(result);
	createfractaldata(result);
	return (result);
}

int					main(int argc, char **argv)
{
	t_fractal	*fractal;

	if (argc != 2)
		usage(argv[0]);
	fractal = init(argv[1]);
	calculatefractal(fractal);
	draw(fractal);
	mlx_loop(fractal->win->mlx_ptr);
	return (0);
}
