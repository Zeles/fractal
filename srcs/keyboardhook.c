/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keyboardhook.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 17:03:12 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 15:49:55 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

void	switchtype(int key, t_fractal *fra)
{
	if (key == 17)
	{
		fra->type++;
		if (fra->type > 2)
			fra->type = 0;
	}
}

void	resize(int key, t_fractal *fra)
{
	if (key == 15)
	{
		editwindow(&fra->win, addivector2d(fra->win->size, 10, 10), "Fractal");
		resizeimage(fra->win->mlx_ptr, &fra->img,
		addivector2d(fra->img->size, 10, 10));
		updateinput(fra);
	}
}

int		keyhook(int key, void *param)
{
	t_fractal *fra;

	fra = (t_fractal*)param;
	switchtype(key, fra);
	resize(key, fra);
	if (key == 53)
		closewindow(fra);
	if (key == 24 || key == 61)
		fra->maxiter += 10;
	if (key == 27 || key == 45)
		if (fra->maxiter > 30)
			fra->maxiter -= 10;
	if (key == 99 || key == 8)
	{
		fra->setcol++;
		if (fra->setcol >= COLORS)
			fra->setcol = 0;
	}
	calculatefractal(fra);
	draw(fra);
	return (0);
}
