/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/07 17:17:39 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/13 20:30:25 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractal.h"

void	*draw(void *arg)
{
	t_fractal	*fra;

	fra = (t_fractal*)arg;
	mlx_clear_window(fra->win->mlx_ptr, fra->win->window);
	mlx_put_image_to_window(fra->win->mlx_ptr, fra->win->window,
	fra->img->img_ptr, 0, 0);
	drawui(fra);
	return (0);
}
