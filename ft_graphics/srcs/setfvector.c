/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setfvector.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 11:51:00 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 11:51:00 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

t_fvector	setfvector(float x, float y, float z)
{
	t_fvector result;

	result.x = x;
	result.y = y;
	result.z = z;
	return (result);
}

void		setfvectorptr(t_fvector *v1, float x, float y, float z)
{
	if (v1 == (void*)0)
		return ;
	v1->x = x;
	v1->y = y;
	v1->z = z;
}
