/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 11:54:45 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 11:54:49 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROTATE_H
# define ROTATE_H
# include "vector.h"

int		ilerp(int start, int end, float t);
int		ilerpd(int start, int end, double t);
float	flerp(float start, float end, float t);

void	xrotate(t_fvector	*point, t_fvector *center, float angle);
void	yrotate(t_fvector	*point, t_fvector *center, float angle);
void	zrotate(t_fvector	*point, t_fvector *center, float angle);

#endif
