NAME = fractol
SRCSFOLDER = ./srcs/
SRCS = main.c \
		updateinput.c mousehook.c zoom.c keyboardhook.c \
		draw.c drawui.c update.c error.c \
		getcolorfractal.c \
		calculatefractal.c drawmolden.c drawjulia.c drawshipburn.c
OBJNAME = $(SRCS:.c=.o)
OBJDIR = ./obj/
OBJ = $(addprefix $(OBJDIR),$(OBJNAME))
INCLUDES = ./includes/
FLAG = -Wall -Wextra -Werror
BUILDFOLDER = ./build/

GRAPHICSFOLDER = ./ft_graphics/
GRAPHICSINCLUDES = $(GRAPHICSFOLDER)includes/
GRAPHICSINK = -I $(GRAPHICSINCLUDES) -L $(GRAPHICSFOLDER) -lftgraphics

LIBFTFOLDER = ./libft/
LIBFTINCLUDES = $(LIBFTFOLDER)
LIBFTINK = -I $(LIBFTINCLUDES) -L $(LIBFTFOLDER) -lft

MLXUTILSFOLDER = ./mlxutils/
MLXUTILSINCLUDE = $(MLXUTILSFOLDER)includes/
MLXUTILSINK = -I $(MLXUTILSINCLUDE) -L $(MLXUTILSFOLDER) -lmlxutils

MLXLINKLINUX = -I /usr/X11/lib -L /usr/X11/lib/ -lmlx -lXext -lX11 -lm -lpthread
MLXLINTMACOS = -I /usr/local/include -L /usr/local/lib -lmlx -framework OpenGL -framework AppKit

all: $(NAME)

$(NAME): $(OBJ)
	$(MAKE) -C $(GRAPHICSFOLDER)
	$(MAKE) -C $(MLXUTILSFOLDER)
	$(MAKE) -C $(LIBFTFOLDER)
	gcc $(FLAG) $(OBJ) $(MLXLINTMACOS) $(MLXUTILSINK) $(GRAPHICSINK) $(LIBFTINK) -o $(NAME)

debugmacos: $(OBJ)
	$(MAKE) -C $(GRAPHICSFOLDER)
	$(MAKE) -C $(MLXUTILSFOLDER)
	$(MAKE) -C $(LIBFTFOLDER)
	mkdir $(BUILDFOLDER)
	gcc -g $(OBJ) $(MLXLINTMACOS) $(MLXUTILSINK) $(GRAPHICSINK) $(LIBFTINK) -I $(INCLUDES) -o $(BUILDFOLDER)$(NAME)macos

debuglinux: $(OBJ)
	$(MAKE) -C $(GRAPHICSFOLDER)
	$(MAKE) -C $(MLXUTILSFOLDER)
	$(MAKE) -C $(LIBFTFOLDER)
	mkdir $(BUILDFOLDER)
	gcc -g $(OBJ) $(GRAPHICSINK) $(MLXUTILSINK) $(MLXLINKLINUX) $(LIBFTINK) -I $(INCLUDES) -o $(BUILDFOLDER)$(NAME)linux

clean:
	$(MAKE) -C $(GRAPHICSFOLDER) clean
	$(MAKE) -C $(MLXUTILSFOLDER) clean
	$(MAKE) -C $(LIBFTFOLDER) clean
	rm -rf $(OBJDIR)
	rm -rf *.o

fclean: clean
	$(MAKE) -C $(GRAPHICSFOLDER) fclean
	$(MAKE) -C $(MLXUTILSFOLDER) fclean
	$(MAKE) -C $(LIBFTFOLDER) fclean
	rm -rf $(BUILDFOLDER)
	rm -rf $(NAME)

$(OBJDIR)%.o:$(SRCSFOLDER)%.c
	mkdir -p $(OBJDIR)
	gcc -g -I $(INCLUDES) -o $@ -c $<

re:	fclean $(NAME)

re_d_macos: fclean debugmacos

re_d_linux: fclean debuglinux
