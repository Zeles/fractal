/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractal.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdaniel <gdaniel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 14:31:40 by gdaniel           #+#    #+#             */
/*   Updated: 2019/02/26 14:52:10 by gdaniel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTAL_H
# define FRACTAL_H
# define THREAD 15
# define COLORS 3
# include <pthread.h>
# include "../ft_graphics/includes/vector.h"
# include "../ft_graphics/includes/color.h"
# include "../ft_graphics/includes/rotate.h"
# include "../mlxutils/includes/mlxutils.h"
# include "../libft/libft.h"

enum			e_fractaltype
{
	MALDENBROT,
	JULIA,
	SHIPBURN
}				fractaltype;
typedef struct	s_maxmin
{
	t_dvector2d	max;
	t_dvector2d	min;
}				t_maxmin;
typedef struct	s_setcolor
{
	t_rgb	colormaxiter;
	t_rgb	start;
	t_rgb	end;
}				t_setcolor;
typedef struct	s_fractal
{
	t_window			*win;
	t_image				*img;
	t_maxmin			maxmin;
	t_ivector2d			startend;
	t_setcolor			colors[COLORS];
	int					setcol;
	int					maxiter;
	int					zoomiter;
	enum e_fractaltype	type;
	t_dvector			mouse;
	t_dvector2d			juliastart;
}				t_fractal;
typedef struct	s_thread
{
	int			iter;
	pthread_t	thread;
	t_fractal	*fra;
}				t_thread;
int				keyhook(int key, void *param);
int				closewindow(void *param);
int				mousehook(int key, int x, int y, void *param);
void			zoom(int key, int x, int y, t_fractal *fra);
void			updateinput(t_fractal *fra);
void			*draw(void *arg);
void			drawui(t_fractal *fra);
void			update(t_fractal *fra);
void			usage(char *argv1);
void			error(char *str);
void			calculatefractal(t_fractal *fra);
void			*calcmolden(void *arg);
void			*calcjulia(void *arg);
void			*calcshipburn(void *arg);
t_rgb			getcolorfractal(t_fractal *fra, int iter);
t_setcolor		setcolor(t_rgb cmi, t_rgb start, t_rgb end);

#endif
